import { Router } from "express";
import { database } from ".";
import { validate_account } from "./models/account";


export var router_account = Router()

router_account.get("/register", (req, res) => res.render("account/register"))
router_account.get("/login", (req, res) => res.render("account/login"))

router_account.post("/register", async (req, res) => {
    const new_account = req.body
    console.log(new_account);

    const errors = validate_account(new_account)
    if (errors.length) {
        return res.render("account/register", { errors })
    }

    database.add_account(new_account)

    res.render("account/login", { success: "You have been registered - you can log in now." })
})


