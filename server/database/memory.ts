import { Account } from "../models/account";
import { DatabaseBackend } from "./generic";

export class InMemoryDatabase extends DatabaseBackend {
    accounts: Account[] = []
    
    async add_account(a: Account) {
        this.accounts.push(a)
    }
}
