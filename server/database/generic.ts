import { Account } from "../models/account";

export abstract class DatabaseBackend {
    
    abstract add_account(account: Account): Promise<void>

}