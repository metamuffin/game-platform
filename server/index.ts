import express from "express";
import { join } from "path/posix";
import { router_account } from "./account";
import { DatabaseBackend } from "./database/generic";
import { InMemoryDatabase } from "./database/memory";

export let database: DatabaseBackend

async function main() {

    const app = express()
    database = new InMemoryDatabase()

    app.set("view engine", "pug")
    app.set("view directory", join(__dirname, "../views"))
    app.disable("x-powered-by")

    app.use(express.json())
    app.use(express.urlencoded({extended: true}))

    app.get("/", (req, res) => res.render("home"))

    app.use("/account", router_account)



    const HOST = process.env.HOST ?? "127.0.0.1"
    const PORT = parseInt(process.env.PORT ?? "8080")
    app.listen(PORT, HOST, () => console.log(`listening on ${HOST}:${PORT}`))
}

main()

