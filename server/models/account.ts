
export interface Account {
    username: string
    password: string
    email?: string

}

const rules: { name: string, check: string }[] = [
    { name: "username required", check: "!!account.username" },
    { name: "max username length", check: "account.username?.length < 64" },
    { name: "min username length", check: "account.username?.length > 4" },
]

export function validate_account(a: Account): string[] {
    let errors: string[] = []
    for (const r of rules) {
        const valid = new Function("account", "return " + r.check)(a)
        if (!valid) {
            errors.push(`${r.name} (${r.check})`)
        }
    }

    return errors
}

